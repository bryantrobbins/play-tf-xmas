// Require the framework and instantiate it
const fastify = require('fastify')({ logger: true })
const path = require('path')

fastify.register(require('@fastify/static'), {
    root: path.join(__dirname, 'public'),
  })

// Declare a route
// Register API routes from external plugin https://www.fastify.io/docs/latest/Guides/Getting-Started/#your-first-plugin
fastify.register(require('./api/animals'), { prefix: '/api/animals' })

// Run the server!
const start = async () => {
  try {
    await fastify.listen({ port: 3000 })
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
}

start()