// Adapted from https://www.fastify.io/docs/latest/Guides/Getting-Started/#your-first-plugin

/**
 * Encapsulates the routes
 * @param {FastifyInstance} fastify  Encapsulated Fastify Instance
 * @param {Object} options plugin options, refer to https://www.fastify.io/docs/latest/Reference/Plugins/#plugin-options
 */
 async function routes (fastify, options) {
    fastify.get('/', async (request, reply) => {
        return ['pig', 'bear', 'man', 'fox']
    })
  }
  
  module.exports = routes